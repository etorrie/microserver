.. MicroServer documentation master file, created by
   sphinx-quickstart on Thu Sep 12 15:19:04 2019.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to MicroServer's documentation!
=======================================

.. toctree::
   :maxdepth: 2
   :caption: Contents:

Setup
----------------------

MicroServer requires `TRlib <https://pypi.org/project/trlib/>`_ to be installed. To launch MicroServer, the following arguments are required:

``--data-dir/-d`` - *required*
    Directory with data files 

``--ip_address/-ip`` - *defaults to 127.0.0.1*
    IP address for MicroServer to serve on 

``--port/-p`` - *defaults to 5005* 
    HTTP port to use

``--lookupkey`` - *defaults to {PATH}*
    format string used as a key for response lookup:
    example: "{%%Host}{%%Server}{PATH}", "{HOST}{PATH}", "{PATH}"
    All the args preceded by %% are header fields in the request
    The only two acceptable arguments which are not header fields are : fqdn (represented by HOST) and the url path (represented by PATH) in a request line.
    Example: given a client request as  << GET /some/resource/location HTTP/1.1\nHost: hahaha.com\n\n >>, if the user wishes the host field and the path to be used for the response lookup
    then the required format will be {%%Host}{PATH}

``--ssl/-ssl``
    Option to enable SSLServer

``--s_port/-s`` - *defaults to 5007*
    SSL port to use

``--delay/-dy`` - *defaults to 0*
    Response delay in ms 

``--timeout/-t``
    Socket delay before connecting in seconds

``--key/-k`` - *defaults to server.pem*
    SSL Key

``--cert/-cert`` - *defaults to server.crt*
    SSL Cert

``--clientverify/-cverify`` - *defaults to False*
    Option to verify client cert

``--clientCA`` - *defaults to /etc/ssl/certs/ca-certificates.crt*
    CA for client certificates

``--load``
    Observer file

``--verbose/-v``
    Toggle verbose outputs

MicroServer can be both launched from the command line or instantiated in another program. If used programmatically, 
the user must also call ``serve_forever()`` on the MicroServer object to start listening on the selected ports.

Usage
----------------------
MicroServer reads in JSON files in the format of the `replay schema <https://bitbucket.org/autestsuite/trafficreplaylibrary/src/master/schema/replay_format.json>`_. 
When a request reaches MicroServer, the server will search through the request's header fields for the headers (and/or path or host) as specified in the ``lookupkey`` argument.
Once the lookupkey info has been retrieved from the incoming request's fields, MicroServer will use the lookupkey to index into the response dictionary and grab the correct response. 
**Because MicroServer uses a standard dictionary internally to hold the responses, having duplicate lookupkeys will cause a response to overwrite the other.**

MicroServer also provides hooks for potential observers to hook into. At the time of writing, only the ``ReadRequestHook`` has been implemented. ``ReadRequestHook`` is invoked at the beginning of the response process.
Here is a sample of the observer file:

.. code-block:: python

    log = open('sslheaders.log', 'w')

    def observe(headers):

        for h in headers.items():
            if h[0].lower().startswith('ssl-'):

                log.write(h[0] + ": " + h[1] + "\n");
        log.write("-\n")
        log.flush()

    Hooks.register(Hooks.ReadRequestHook, observe)

TODOs
----------------------
* Better organize the arguments
* More helpful verbose outputs
* Simplify HTTP reading/writing logic (it is unnecessarily hand-rolled at the moment)


Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
