v1.0.4  Oct. 23, 2019:
    [Pr #10] Allowing keep-alive instead of always closing the connection
    Fix to setup.py logic to not crash on because of missing dependancy
v1.0.3  Sep. 26, 2019:
    [Pr #8] Fixed issue with default sigINT handler not existing odd CI cases.
v1.0.2  Aug. 6, 2019:
    Fixed a bug where Microserver isn't terminating on sigINT by force closing every connection after response
v1.0.1  Jan. 4, 2019:
    Fixed bugs where SSL server was starting on the nonSSL server's port and fixed up the read hook logic
v1.0.0  Dec. 3, 2018:
    Initial release